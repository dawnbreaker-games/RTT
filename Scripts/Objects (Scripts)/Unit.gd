extends Node2D
class_name Unit

@export var moveSpeed : float
@export var maxHp : float
@export var hpRegen : float
@export var attackSpeed : float
@export var attackDamage : int
@export var attackRange : int
@export var team : int
@export var rigid : RigidBody2D
@export var sprite : Sprite2D
@export var selectedSprite : Sprite2D
@export var attackRaycast : RayCast2D
@export var enemyShapeCast : ShapeCast2D
@export var hpRange : Range
@export var radius : float
@export var beamScenePath : String
var destination
var moving : bool
var hp : float
var attackTimer : float
var aggressive = true
var targetUnit : Unit
var dead : bool

func _ready ():
	if !(self is AggressiveUnit):
		AggressiveUnit.nonAggressiveUnits.append(self)
	hp = maxHp
	if team == 0:
		rigid = get_child(0)
		sprite = rigid.get_child(1)
		selectedSprite = rigid.get_child(2)
		attackRaycast = sprite.get_child(0)
		enemyShapeCast = rigid.get_child(3)
		hpRange = rigid.get_child(4)
		Player.units.append(self)
	rigid.body_exited.connect(OnBodyExited)

func _process (deltaTime : float):
	TakeDamage (-hpRegen * deltaTime)
	if !moving || absf(rad_to_deg((destination - rigid.global_position).angle_to(rigid.linear_velocity))) >= 90:
		rigid.linear_velocity = Vector2.ZERO
		moving = false
	HandleAttacking ()

func HandleAttacking ():
	var previousAbleToAttack = attackTimer <= 0
	if !previousAbleToAttack:
		attackTimer -= GameManager.deltaTime
	if aggressive && attackTimer <= 0:
		enemyShapeCast.force_shapecast_update()
		var hitCount = enemyShapeCast.get_collision_count()
		if hitCount > 0:
			if targetUnit == null || !TryToAttackUnit(targetUnit, previousAbleToAttack):
				for i in range(hitCount):
					var collider = enemyShapeCast.get_collider(i)
					if TryToAttackUnit(collider.get_parent(), previousAbleToAttack):
						return

func TryToAttackUnit (unit : Unit, previousAbleToAttack : bool):
	sprite.look_at(unit.rigid.global_position)
	sprite.global_rotation_degrees += 90
	attackRaycast.force_raycast_update()
	var collider = attackRaycast.get_collider()
	if collider != null:
		var hitUnit = collider.get_parent()
		if hitUnit.team != team:
			var beam = load(beamScenePath).instantiate()
			var points : PackedVector2Array
			var hitPoint = attackRaycast.get_collision_point()
			points.append(rigid.global_position + (hitPoint - rigid.global_position).normalized() * unit.radius)
			points.append(hitPoint)
			beam.points = points
			get_tree().current_scene.add_child(beam)
			Attack (hitUnit, previousAbleToAttack)
			return true
	return false

func Attack (unit : Unit, previousAbleToAttack : bool):
	unit.TakeDamage (attackDamage)
	if !previousAbleToAttack:
		attackTimer += 1 / attackSpeed
	else:
		attackTimer = 1 / attackSpeed

func TakeDamage (amount : float):
	hp = clampf(hp - amount, 0, maxHp)
	if !dead:
		hpRange.value = hp / maxHp
		if hp == 0:
			dead = true
			Die ()

func Die ():
	Player.units.erase(self)
	if Player.units.size() == 0:
		GameManager.Save ()
		GameManager.OnPreChangeScene ()
		get_tree().reload_current_scene()
		return
	Player.selected.erase(self)
	AggressiveUnit.nonAggressiveUnits.erase(self)
	queue_free()
	get_tree().current_scene.remove_child(self)

func MoveTo (moveTo : Vector2):
	destination = moveTo
	rigid.linear_velocity = (moveTo - rigid.global_position).normalized() * moveSpeed
	sprite.look_at(moveTo)
	sprite.global_rotation_degrees += 90
	moving = true

func OnBodyExited (node : Node2D):
	if moving:
		sprite.look_at(destination)
		sprite.global_rotation_degrees += 90

func OnStatsChanged ():
	attackRaycast.target_position = Vector2.DOWN * attackRange
	enemyShapeCast.shape.set("radius", attackRange)
	if moving:
		rigid.linear_velocity = (destination - rigid.global_position).normalized() * moveSpeed
