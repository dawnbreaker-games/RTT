extends Unit
class_name SpawnerUnit

@export var spawnUnitPath : String
@export var spawnInterval : float
var spawnTimer : float

func _ready ():
	super._ready ()
	spawnTimer = spawnInterval

func _process (deltaTime : float):
	super._process (deltaTime)
	HandleSpawning (deltaTime)

func HandleSpawning (deltaTime : float):
	spawnTimer -= deltaTime
	if spawnTimer <= 0:
		spawnTimer += spawnInterval
		var unit = load(spawnUnitPath)
		unit = unit.instantiate()
		get_tree().current_scene.add_child(unit)
		unit.rigid.global_position = rigid.global_position + VectorExtensions.Random(FloatRange.New(radius + unit.radius, radius + unit.radius))
