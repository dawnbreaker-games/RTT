extends Unit
class_name AggressiveUnit

static var nonAggressiveUnits : Array[Unit]
var attackUnit : Unit

func _process (deltaTime : float):
	var closestUnit = null
	var closestUnitDistanceSqr = INF
	for unit in nonAggressiveUnits:
		var distanceSqr = (unit.rigid.global_position - rigid.global_position).length_squared()
		if distanceSqr < closestUnitDistanceSqr:
			closestUnitDistanceSqr = distanceSqr
			closestUnit = unit
	attackUnit = null
	HandleAttacking ()
	if attackUnit == null && closestUnit != null:
		MoveTo (closestUnit.rigid.global_position)

func Attack (unit : Unit, previousAbleToAttack : bool):
	super.Attack (unit, previousAbleToAttack)
	attackUnit = unit
