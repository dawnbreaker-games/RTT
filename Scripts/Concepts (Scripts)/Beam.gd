extends Node2D
class_name Beam

@export var duration : float
@export var canvasItem : CanvasItem
var timeRemaining : float

func _ready ():
	timeRemaining = duration

func _process (deltaTime : float):
	timeRemaining = clampf(timeRemaining - deltaTime, 0, duration)
	canvasItem.modulate.a = timeRemaining / duration
	if timeRemaining <= 0:
		free()
