extends Node2D
class_name Selection2

@export var shapeCast : ShapeCast2D
static var instance : Selection2

func _ready ():
	instance = self

func _process (deltaTime : float):
	shapeCast.force_shapecast_update()
	var deselected : Array[Unit]
	deselected.append_array(Player.selected)
	Player.selected.clear()
	for i in range(shapeCast.get_collision_count()):
		var unit = shapeCast.get_collider(i).get_parent()
		Player.selected.append(unit)
		unit.selectedSprite.visible = true
		deselected.erase(unit)
	for unit in deselected:
		unit.selectedSprite.visible = false
