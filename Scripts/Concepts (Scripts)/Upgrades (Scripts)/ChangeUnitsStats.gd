extends Upgrade
class_name ChangeUnitsStats

@export var addToStatName : String
@export var addToStatValue : float
@export var subtractFromStatName : String
@export var subtractFromStatValue : float

func OnResearched ():
	super.OnResearched ()
	for unit in Player.units:
		unit.set(addToStatName, unit.get(addToStatName) + addToStatValue)
		unit.set(subtractFromStatName, unit.get(subtractFromStatName) - subtractFromStatValue)
		unit.OnStatsChanged ()
	var unit = Player.LOCAL_UNIT_SCENE.instantiate()
	unit.set(addToStatName, unit.get(addToStatName) + addToStatValue)
	unit.set(subtractFromStatName, unit.get(subtractFromStatName) - subtractFromStatValue)
	unit.OnStatsChanged ()
	Player.LOCAL_UNIT_SCENE.pack(unit)
	unit.free()
