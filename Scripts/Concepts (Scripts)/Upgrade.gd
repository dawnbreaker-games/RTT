extends HBoxContainer
class_name Upgrade

@export var researchDuration : int
@export var multiplyResearchDurationOnResearched : float
@export var addToResearchDurationOnResearched : int
@export var label : Label
@export var setDefaultButton : Button
@export var addResearcherButton : Button
@export var removeResearcherButton : Button
static var instances : Array[Upgrade]
static var default : Upgrade
static var researchersToReassign : Array[Upgrade]
var researchers : int
var myResearchersToReassign : int
var researchTimer : float
var initLabelText : String

func _ready ():
	instances.append(self)
	addResearcherButton.disabled = researchersToReassign.size() == 0
	removeResearcherButton.disabled = myResearchersToReassign == researchers
	initLabelText = label.text
	label.text += "\n" + str(int(researchTimer)) + "/" + str(int(researchDuration))
	set_process(researchers > 0)

func _process (deltaTime : float):
	researchTimer += researchers * GameManager.deltaTime
	if researchTimer >= researchDuration:
		OnResearched ()
	label.text = initLabelText + "\n" + str(int(researchTimer)) + "/" + str(int(researchDuration))

func OnResearched ():
	researchTimer -= researchDuration
	researchDuration *= multiplyResearchDurationOnResearched
	researchDuration += addToResearchDurationOnResearched

func SetDefault ():
	if default != null:
		default.setDefaultButton.disabled = false
	default = self
	setDefaultButton.disabled = true

func AddResearcher ():
	if researchersToReassign.size() > 0:
		var upgrade = researchersToReassign[0]
		researchersToReassign.remove_at(0)
		upgrade.researchers -= 1
		upgrade.myResearchersToReassign -= 1
	researchers += 1
	removeResearcherButton.disabled = false
	if researchersToReassign.size() == 0:
		for upgrade in instances:
			if upgrade != null:
				upgrade.addResearcherButton.disabled = true 
	set_process(true)

func RemoveResearcher ():
	researchersToReassign.append(self)
	myResearchersToReassign += 1
	removeResearcherButton.disabled = myResearchersToReassign == researchers
	for upgrade in instances:
		upgrade.addResearcherButton.disabled = false
	set_process(researchers > 0)
