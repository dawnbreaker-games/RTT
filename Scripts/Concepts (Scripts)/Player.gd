extends Node2D
class_name Player

@export var targetUnitRaycast : RayCast2D
@export var researchers : int
@export var initDefaultUpgrade : Upgrade
@export var multiplyTimeScaleWhileSpedUp : float
static var instance : Player
static var units : Array[Unit]
static var selected : Array[Unit]
const SELECTION_SCENE = preload('res://Scenes/Concepts (Scenes)/Selection.tscn')
const LOCAL_UNIT_SCENE = preload('res://Scenes/Objects (Scenes)/Unit Player 1.tscn')
var selectionStartPosition : Vector2
var placeableUnits : int
var placingUnit : bool

func _ready ():
	instance = self
	initDefaultUpgrade.SetDefault ()
	initDefaultUpgrade.AddResearcher ()

func _process (deltaTime : float):
	if Input.is_action_just_pressed('Speed Up Time'):
		Engine.time_scale *= multiplyTimeScaleWhileSpedUp
	elif Input.is_action_just_released('Speed Up Time'):
		Engine.time_scale /= multiplyTimeScaleWhileSpedUp

func _input (event : InputEvent):
	if event is InputEventMouseButton:
		if Input.is_action_just_pressed('Left Click'):
			if placingUnit:
				var unit = LOCAL_UNIT_SCENE.instantiate()
				unit.global_position = RectExtensions.ClampPoint(GameManager.instance.arenaRect, get_global_mouse_position())
				get_tree().current_scene.add_child(unit)
				placeableUnits -= 1
				placingUnit = false
			else:
				for unit in selected:
					unit.selectedSprite.visible = false
				selected.clear()
				selectionStartPosition = get_global_mouse_position()
				Selection2.instance = SELECTION_SCENE.instantiate()
				Selection2.instance.global_position = selectionStartPosition
				Selection2.instance.scale = Vector2.ZERO
				get_tree().current_scene.add_child(Selection2.instance)
		elif Input.is_action_just_pressed('Right Click'):
			placingUnit = false
			var mousePosition = get_global_mouse_position()
			targetUnitRaycast.global_position = mousePosition
			targetUnitRaycast.force_raycast_update()
			var targetUnit : Unit
			var collider = targetUnitRaycast.get_collider()
			if collider != null:
				targetUnit = collider.get_parent()
			for unit in selected:
				unit.targetUnit = targetUnit
				unit.MoveTo (mousePosition)
		elif Input.is_action_just_released('Left Click') && Selection2.instance != null:
			Selection2.instance.queue_free()
			get_tree().current_scene.remove_child(Selection2.instance)
	elif event is InputEventKey:
		if Input.is_action_just_pressed('New Unit') && placeableUnits > 0:
			placingUnit = !placingUnit
			if placingUnit && Selection2.instance != null:
				Selection2.instance.queue_free()
				get_tree().current_scene.remove_child(Selection2.instance)
		elif Input.is_action_just_pressed('Reset'):
			GameManager.OnPreChangeScene ()
			get_tree().reload_current_scene()
	elif event is InputEventMouseMotion && Selection2.instance != null:
		var mousePosition = get_global_mouse_position()
		var newSize = mousePosition - selectionStartPosition
		newSize /= 20
		Selection2.instance.scale = newSize
