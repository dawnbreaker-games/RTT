class_name VectorExtensions

static func Deg2Rad (v : Vector2):
	return Vector2(deg_to_rad(v.x), deg_to_rad(v.y))

static func Rad2Deg (v : Vector2):
	return Vector2(rad_to_deg(v.x), rad_to_deg(v.y))

static func Rotate90 (v : Vector2):
	return Vector2(-v.y, v.x)

static func Rotate270 (v : Vector2):
	return Vector2(v.y, -v.x)

static func Snap (v : Vector2, interval : float):
	return Vector2(MathExtensions.SnapToInterval(v.x, interval), MathExtensions.SnapToInterval(v.y, interval))

static func SetY (v : Vector2, y : float):
	return Vector2(v.x, y)

static func FlipY (v : Vector2):
	return Vector2(v.x, -v.y)

static func GetFacingAngle (v : Vector2):
	v = v.normalized()
	return rad_to_deg(atan2(v.y, v.x)) + 90

static func FromFacingAngle (angle : float):
	return Vector2(cos(deg_to_rad(angle)), sin(deg_to_rad(angle))).normalized()

static func Random (lengthRange : FloatRange):
	return FromFacingAngle(randf_range(0, 360)) * lengthRange.Get(randf())
