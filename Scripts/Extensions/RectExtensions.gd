class_name RectExtensions

static func ClampPoint (rect : Rect2i, point : Vector2):
	var absRect = rect.abs()
	return point.clamp(absRect.position, absRect.end)

static func RandomPointOnBounds (rect : Rect2i):
	var absRect = rect.abs()
	var size = absRect.size
	var perimeter = size.x * 2 + size.y * 2
	var distanceAlongPerimeter = randf_range(0, perimeter)
	if distanceAlongPerimeter < size.x:
		return Vector2(absRect.position.x + distanceAlongPerimeter, absRect.position.y)
	else:
		distanceAlongPerimeter -= size.x
		if distanceAlongPerimeter < size.y:
			return Vector2(absRect.end.x, absRect.position.y + distanceAlongPerimeter)
		else:
			distanceAlongPerimeter -= size.y
			if distanceAlongPerimeter < size.x:
				return Vector2(absRect.end.x - distanceAlongPerimeter, absRect.end.y)
			else:
				distanceAlongPerimeter -= size.x
				return Vector2(absRect.position.x, absRect.end.y - distanceAlongPerimeter)
