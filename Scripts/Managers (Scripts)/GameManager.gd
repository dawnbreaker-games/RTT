extends Node2D
class_name GameManager

@export var waveInterval : float
@export var arenaRect : Rect2i
@export var isDuel : bool
@export var timeLabel : Label
@export var bestTimeLabel : Label
static var deltaTime : float
static var currentLevelSceneIndex = 1
static var instance : GameManager
static var time : float
static var bestTime : int
static var nextWaveTimer : float
static var waveCount : int
const WAVE_UNIT = preload("res://Scenes/Objects (Scenes)/Unit Wave Player.tscn")
const SAVE_FILE_PATH = "user://Save Data.cfg"

func _ready ():
	instance = self
	Engine.time_scale = 1
	nextWaveTimer += waveInterval
	Load ()
	bestTimeLabel.text = str(bestTime)

func _process (deltaTime : float):
	self.deltaTime = deltaTime
	time += deltaTime
	timeLabel.text = str(int(time))
	nextWaveTimer -= deltaTime
	if nextWaveTimer <= 0:
		nextWaveTimer += waveInterval
		waveCount += 1
		for i in range(waveCount):
			if isDuel:
				var unit = WAVE_UNIT.instantiate()
				unit.global_position = Vector2(arenaRect.position.x, randf_range(arenaRect.position.y, arenaRect.end.y))
				get_tree().current_scene.add_child(unit)
				unit = WAVE_UNIT.instantiate()
				unit.global_position = Vector2(arenaRect.end.x, randf_range(arenaRect.position.y, arenaRect.end.y))
				get_tree().current_scene.add_child(unit)
			else:
				var unit = WAVE_UNIT.instantiate()
				unit.global_position = RectExtensions.RandomPointOnBounds(arenaRect)
				get_tree().current_scene.add_child(unit)

func NextLevel ():
	currentLevelSceneIndex += 1
	var scenePath = "res://Scenes/" + str(currentLevelSceneIndex) + ".tscn"
	if !ResourceLoader.exists(scenePath):
		currentLevelSceneIndex -= 1
		return
	OnPreChangeScene ()
	get_tree().change_scene_to_file(scenePath)

func PreviousLevel ():
	currentLevelSceneIndex -= 1
	var scenePath = "res://Scenes/" + str(currentLevelSceneIndex) + ".tscn"
	if !ResourceLoader.exists(scenePath):
		currentLevelSceneIndex += 1
		return
	OnPreChangeScene ()
	get_tree().change_scene_to_file(scenePath)

static func OnPreChangeScene ():
	time = 0
	nextWaveTimer = 0
	waveCount = 0
	Player.units.clear()
	Player.selected.clear()
	AggressiveUnit.nonAggressiveUnits.clear()
	Upgrade.instances.clear()
	Upgrade.researchersToReassign.clear()
	
static func Save ():
	var config = ConfigFile.new()
	if int(time) > bestTime:
		bestTime = time
		config.set_value("", "bestTime", bestTime)
		config.save(SAVE_FILE_PATH)

static func Load ():
	var config = ConfigFile.new()
	var error = config.load(SAVE_FILE_PATH)
	if error == OK:
		for section in config.get_sections():
			bestTime = config.get_value(section, "bestTime", 0)
